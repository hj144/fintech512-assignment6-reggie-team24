
import java.sql.*;

public abstract class Baseclass extends DatabaseSupport {
    public abstract Baseclass create (ResultSet result);
    public abstract String getWhere();
    public static Baseclass find(Baseclass baseclass){
        String derivedtype = baseclass.getClass().getName();
        try {
            Statement statement = getConnectionStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM "+ derivedtype + " WHERE" + baseclass.getWhere());
            if (result.next() == false)
                return null;
            Baseclass bc = baseclass.create(result);
           conn.close();
           return bc;
        } catch (Exception ex) {
            closeConnection();
            return null;
        }
    }
}
