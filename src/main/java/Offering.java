import java.sql.*;

public class Offering extends Baseclass {
	private int id;
	private Course course;
	private String daysTimes;
	public String getWhere () {
		return "id = " + id;
	}

	public Offering (int id ) {
		this.id = id;
	}
	public static Offering create(Course course, String daysTimesCsv) throws Exception {
		try {
			Statement statement = getConnectionStatement();
			if (statement == null) throw null;

			ResultSet result = statement.executeQuery("SELECT MAX(id) FROM offering");
			result.next();
			int newId = 1 + result.getInt(1);

			statement.executeUpdate("INSERT INTO offering VALUES (" + newId + ",'"
					+ course.getName() + "','" + daysTimesCsv + "')");
			return new Offering(newId, course, daysTimesCsv);

		} finally {
			closeConnection();
		}
	}
	public static Offering find (int id) {
		Offering offering = new Offering(id);
		return (Offering) find (offering);
	}
	public Baseclass create (ResultSet result) {
		String courseName = null;
		Course course = null;
		String dateTime = null;
		try {
			courseName = result.getString("name");
			 course = Course.find(courseName);
			dateTime = result.getString("daysTimes");
		} catch (SQLException e) {
			e.printStackTrace();
		}


		return new Offering(id, course, dateTime);
	}


	public void update() throws Exception {
		Connection conn = null;

		try {
			Statement statement = getConnectionStatement();

			statement.executeUpdate("DELETE FROM offering WHERE id=" + id + "");
			statement.executeUpdate("INSERT INTO offering VALUES(" + id + ",'" + course.getName()
					+ "','" + daysTimes + "')");
		} finally {
			try {
				conn.close();
			} catch (Exception ignored) {
			}
		}
	}

	public Offering(int id, Course course, String daysTimesCsv) {
		this.id = id;
		this.course = course;
		this.daysTimes = daysTimesCsv;
	}

	public int getId() {
		return id;
	}

	public Course getCourse() {
		return course;
	}

	public String getDaysTimes() {
		return daysTimes;
	}

	public String toString() {
		return "Offering " + getId() + ": " + getCourse() + " meeting " + getDaysTimes();
	}
}
