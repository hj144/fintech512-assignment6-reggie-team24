import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseSupport {
    static String dbName="reggie";
    static String connectionURL = "jdbc:derby:" + dbName + ";create=true";

    static Connection conn = null;

    public static Statement getConnectionStatement() {
        try {
            conn = DriverManager.getConnection(connectionURL);
            return conn.createStatement();
        } catch (Exception e) {
            return null;
        }
    }

    public static void closeConnection() {
        try {
            conn.close();
        }
        catch (Exception ignored) {
        }
    }

}
