import java.sql.*;

public class Course extends DatabaseSupport {
	private String name;
	private int credits;

	public static Course create(String name, int credits) throws Exception {
		try {
			Statement statement = getConnectionStatement();
			if (statement == null) throw null;
			statement.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
			statement.executeUpdate("INSERT INTO course VALUES ('" + name + "', " + credits + ")");
			return new Course(name, credits);
		} finally {
			closeConnection();
		}
	}

	public static Course find(String name) {
		try {
			Statement statement = getConnectionStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM course WHERE name = '" + name
					+ "'");
			if (!result.next())
				return null;

			int credits = result.getInt("Credits");
			return new Course(name, credits);
		} catch (Exception ex) {
			return null;
		} finally {
			closeConnection();
		}

	}

	public void update() throws Exception {
		Connection conn = null;

		try {
			Statement statement = getConnectionStatement();

			statement.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
			statement.executeUpdate("INSERT INTO course VALUES('" + name + "'," + credits + ")");
		} finally {
			try {
				conn.close();
			} catch (Exception ignored) {
			}
		}
	}

	Course(String name, int credits) {
		this.name = name;
		this.credits = credits;
	}

	public int getCredits() {
		return credits;
	}

	public String getName() {
		return name;
	}
}
